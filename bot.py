#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

import logging
import os

# TelegramBot import
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

# Selenium import
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located

# Fantacalcio League URL
url = 'https://leghe.fantacalcio.it/serie-a-casa-loro/formazioni'

# Webdriver settings
firefox_driver_path = '/home/accoteo/Downloads/fanta-penalty/geckodriver'
firefox_options = Options()
firefox_options.add_argument('--headless')

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

PORT = int(os.environ.get('PORT', '8443'))


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def help(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text('See ')

def penalties(update, context):
    teamArr = []
    penaltyArr = [] 
    
    webdriver = webdriver.Firefox(
        executable_path=firefox_driver_path,
        options=firefox_options
    )
    
    with webdriver as driver:
        # set timeout time 
        wait = WebDriverWait(driver, 5)

        # retrive url in headless browser
        driver.get(url)

        # wait for JS loading
        wait.until(presence_of_element_located((By.ID, "formationTable")))

        # get div list in which penalty is contained
        status = driver.find_elements_by_class_name('note')

        #get div list in which team name is contained
        teams = driver.find_elements_by_class_name('media-heading.ellipsis')
        
        # "Recuperata dal sistema" = true (penalty), otherwise = false (nothing)
        for state in status:
          state = state.get_attribute('innerHTML')
          if "Recuperata" in state:
            penaltyArr.append(True)
          else:
            penaltyArr.append(False)

        # build teams array in accord to penaltyArr[i]=True
        i = 0
        for team in teams:
          if penaltyArr[i] == True:
            teamArr.append(team.get_attribute('innerHTML'))
          i += 1
        
        # must close the driver after task finished
        driver.close()

        # build return string
        if len(teamArr) != 0:
          ret = join(map(str, teamArr))
        else:
          ret = "Nessuna penalità da assegnare!"

        # return 
        update.message.reply_text(ret)

def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(TOKEN, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("penalties", start))
    dp.add_handler(CommandHandler("help", help))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_webhook(listen="0.0.0.0",
                          port=PORT,
                          url_path=TOKEN)
    # updater.bot.set_webhook(url=settings.WEBHOOK_URL)
    updater.bot.set_webhook(APP_NAME + TOKEN)

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
