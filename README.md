# ⚽️ [BETA] FANTACALCIO PENALTIES BOT ⚽️
From the moment the FSP (Fantacalcio Service Provider, https://leghe.fantacalcio.it) doesn't offer the possibility to assign automatically penalties to those partecipats that don't insert the line-up whitin the time, I decide to build a Telegram bot in order to facilitate this operation.

## ⚙️ How it works ⚙️
The web page parsing is performed in Python, with Selenium library. It's necessary the usage of Selenium because the FSP doesn't provide any API and it uses JavaScript to build and populate the web page.<br>
_Resources:_
- https://medium.com/level-up-programming/how-to-scrap-data-from-javascript-based-website-using-python-selenium-and-headless-web-driver-531c7fe0c01f
- https://pypi.org/project/selenium/

The bot is built and deployed on the base of those two guides.<br>
_Resources:_
- https://www.codementor.io/@karandeepbatra/part-1-how-to-create-a-telegram-bot-in-python-in-under-10-minutes-19yfdv4wrq
- https://www.codementor.io/@karandeepbatra/part-2-deploying-telegram-bot-for-free-on-heroku-19ygdi7754


## 👉 How to use 👈
Just add **@FantaPenaltiesBot** to your Telegram Fantaleague chat and type the **/penalties** command! It returns the list of teams to which a penalty will be applied during the scoreboard calculus.<br>
_**NB:**_ the command must be launched whitin the current championship round, between the line-up delivery deadline and the calculation of the scoreboard. In case you do not, unexpected behaviours may occur.

## 📈 Possible enhancements 📈
- Make the bot available for every league:
    - Create a command (**/configure**) that allows to configure the league path, which is in the form https://leghe.fantacalcio.it/YOUR-LEAGUE-NAME/formazioni.
- Maximize the automation:
    - Create a command (**/calendar**) that takes a list of dates and hours (possibly in the format DD/MM/AAAA-hh:mm) and automatically, when it's time, returns the list of teams which deserve the penalty.
- Improve overall performances;
- Correct the unexpected behaviour in case the commands aren't launched with the correct timing.

## ❗️ Disclaimer ❗️
This is my first approach with Python, its libraries and Telegram APIs. I tried to my best for the sole purpose of learning new stuff. <br>
Feel free to clone, modify, improve or suggest improvements!
