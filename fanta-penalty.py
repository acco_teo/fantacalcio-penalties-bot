from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located

url = 'https://leghe.fantacalcio.it/serie-a-casa-loro/formazioni'
firefox_driver_path = '/home/accoteo/Downloads/fanta-penalty/geckodriver'

firefox_options = Options()
firefox_options.add_argument('--headless')

webdriver = webdriver.Firefox(
  executable_path=firefox_driver_path,
  options=firefox_options
)

teamArr = []
penaltyArr = [] 

with webdriver as driver:
    # set timeout time 
    wait = WebDriverWait(driver, 5)

    # retrive url in headless browser
    driver.get(url)

    # wait for JS loading
    wait.until(presence_of_element_located((By.ID, "formationTable")))

    # get div list in which penalty is contained
    status = driver.find_elements_by_class_name('note')

    #get div list in which team name is contained
    teams = driver.find_elements_by_class_name('media-heading.ellipsis')
    
    # "Recuperata dal sistema" = true (penalty), otherwise = false (nothing)
    for state in status:
      state = state.get_attribute('innerHTML')
      if "Recuperata" in state:
        penaltyArr.append(True)
      else:
        penaltyArr.append(False)
    #print(penaltyArr)

    # build teams array in accord to penaltyArr[i]=True
    i = 0
    for team in teams:
      if penaltyArr[i] == True:
        teamArr.append(team.get_attribute('innerHTML'))
      i += 1
    #print(teamArr)
    
    # must close the driver after task finished
    driver.close()

    # build return string
    if len(teamArr) != 0:
      ret = join(map(str, teamArr))
    else:
      ret = "Nessuna penalità da assegnare!"

    print(ret)